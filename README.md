# Red Hat Single Sign-On (RHSSO) on OpenShift Demo
<!-- TOC -->

- [Red Hat Single Sign-On (RHSSO) on OpenShift Demo](#red-hat-single-sign-on-rhsso-on-openshift-demo)
  - [Setup Red Hat Single Sign-On with Operator](#setup-red-hat-single-sign-on-with-operator)
  - [Create Realm and User](#create-realm-and-user)
  - [Direct Grant Access](#direct-grant-access)
      - [RHSSO Web Administrator Console](#rhsso-web-administrator-console)
      - [Import Configuration](#import-configuration)
      - [RHSSO Operator](#rhsso-operator)
    - [Test with cURL](#test-with-curl)
  - [Client Credentials Grant](#client-credentials-grant)
    - [Create Client](#create-client)
      - [RHSSO Web Administrator Console](#rhsso-web-administrator-console-1)
      - [Import Configuration](#import-configuration-1)
      - [RHSSO Operator](#rhsso-operator-1)
    - [Test with cURL](#test-with-curl-1)
  - [Integration with Kong](#integration-with-kong)

<!-- /TOC -->
## Setup Red Hat Single Sign-On with Operator
- Create project
```bash
RHSSO_NS=api-gateway
oc new-project $RHSSO_NS --display-name="Red Hat Single Sign-On and API Gateway"
```
- Install **Red Hat Single Sign-On Operator** on project
  
  ![](images/rhsso-operator.png)
  
- Create RHSSO instance
```bash
export RHSSO_NS=gateway
oc apply -f keycloak/keycloak.yaml -n $RHSSO_NS
```
- Check for status
```bash
watch oc get pods -n $RHSSO_NS
```
Sample output

```bash
NAME                                   READY   STATUS    RESTARTS   AGE
keycloak-0                             1/1     Running   0          2m49s
keycloak-postgresql-78f85c458b-mhpqr   1/1     Running   0          2m45s
rhsso-operator-bf8c9dbb5-xrxmt         1/1     Running   0          4m47s
```
- Check for RHSSO administrator user and password
```bash
KEYCLOAK_INSTANCE=$(oc get keycloak --no-headers -n $RHSSO_NS| head -n 1 | awk '{print $1}')
KEYCLOAK_SECRET=$(oc get keycloak $KEYCLOAK_INSTANCE -n $RHSSO_NS --output="jsonpath={.status.credentialSecret}")
ADMIN_USERNAME=$(oc get secret $KEYCLOAK_SECRET -n $RHSSO_NS -o go-template='{{range $k,$v := .data}}{{printf "%s: " $k}}{{if not $v}}{{$v}}{{else}}{{$v | base64decode}}{{end}}{{"\n"}}{{end}}'|grep ADMIN_USERNAME | awk -F': ' '{print $2}')
ADMIN_PASSWORD=$(oc get secret $KEYCLOAK_SECRET -n $RHSSO_NS -o go-template='{{range $k,$v := .data}}{{printf "%s: " $k}}{{if not $v}}{{$v}}{{else}}{{$v | base64decode}}{{end}}{{"\n"}}{{end}}'|grep ADMIN_PASSWORD | awk -F': ' '{print $2}')
echo "Administrator: $ADMIN_USERNAME"
echo "Password: $ADMIN_PASSWORD"
```
## Create Realm and User
- Create Realm **demo**
```bash
oc apply -f keycloak/realm.yaml -n $RHSSO_NS
```
- Check for RHSSO's URLs
```bash
KEYCLOAK_URL=https://$(oc get route keycloak -n $RHSSO_NS --template='{{ .spec.host }}')/auth
KEYCLOAK_ADMIN_CONSOLE=$KEYCLOAK_URL/admin
KEYCLOAK_ACCOUNT_CONSOLE=$KEYCLOAK_URL/realms/$REALM/account
echo "URL: $KEYCLOAK_ADMIN_CONSOLE"
```
- Use browser to open $KEYCLOAK_ADMIN_CONSOLE and login with Admin user and password 
  
  ![](images/rhsso-console.png)
  
- Create sample user **jdoe** to realm **demo**
```bash
# WIP
# Remark: not work via Operator. stuck at Phase: reconciled
oc create -f keycloak/jdoe.yaml -n $RHSSO_NS
```
## Direct Grant Access
OpenID Client Authorization Code Flow can be enabled by select **Access Type with Public** and **Direct Grant Access is enabled**. This is Resource Owner password credential grant

#### RHSSO Web Administrator Console
- Login to RHSSO using URL *$KEYCLOAK_ADMIN_CONSOLE* with administrator user and password
- Select Settings->Clients
  - Input Client ID
  - Select Access Type to Public
  - Enable Direct Grant Access
  - Click Save
    
    ![](images/client-direct-grant-access-create.png)

#### Import Configuration
- Login to RHSSO using URL *$KEYCLOAK_ADMIN_CONSOLE* with administrator user and password
- Click Settings->Clients->Create->Select File
- Select [keycloak/client-direct-grant-access.json](keycloak/client-direct-grant-access.json)

#### RHSSO Operator
WIP
### Test with cURL
```bash
CLIENT_ID=client-direct-grant-access
USER=jdoe
PASSWORD=openshift
REALM=demo
curl --location --request POST $KEYCLOAK_URL/realms/$REALM/protocol/openid-connect/token \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode client_id=$CLIENT_ID \
--data-urlencode username=$USER \
--data-urlencode password=$PASSWORD \
--data-urlencode scope=email \
--data-urlencode grant_type=password
```
Extract **access_token** with jq
```bash
curl -s --location --request POST $KEYCLOAK_URL/realms/$REALM/protocol/openid-connect/token \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode client_id=$CLIENT_ID \
--data-urlencode username=$USER \
--data-urlencode password=$PASSWORD \
--data-urlencode scope=email \
--data-urlencode grant_type=password | jq '.access_token'
```

## Client Credentials Grant
OpenID Client Credentials Grant can be enabled by select **Access Type with Confidential** and **Service Accounts is enabled**

### Create Client
#### RHSSO Web Administrator Console
- Login to RHSSO using URL *$KEYCLOAK_ADMIN_CONSOLE* with administrator user and password
- Select Settings->Clients
  - Input Client ID
  - Select Access Type to Confidential
  - Enable Service Accounts
  - Click Save
  
    ![](images/client-credentials-create.png)

  - Select Credentials Tab to check for secret

    ![](images/client-credentials-secret.png)

#### Import Configuration
- Login to RHSSO using URL *$KEYCLOAK_ADMIN_CONSOLE* with administrator user and password
- Click Settings->Clients->Create->Select File
- Select [keycloak/client-credentials-demo.json](keycloak/client-credentials-demo.json)
#### RHSSO Operator
WIP
### Test with cURL
```bash
CLIENT_ID=client-credentials-demo
CLIENT_CREDENTIAL=e2a3a276-23aa-4a69-9a72-9a22e55ec07d
REALM=demo
curl --location --request POST $KEYCLOAK_URL/realms/$REALM/protocol/openid-connect/token \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode client_id=$CLIENT_ID \
--data-urlencode client_secret=$CLIENT_CREDENTIAL \
--data-urlencode scope=email \
--data-urlencode grant_type=client_credentials
```
Extract **access_token** with jq
```bash
curl -s --location --request POST $KEYCLOAK_URL/realms/$REALM/protocol/openid-connect/token \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode client_id=$CLIENT_ID \
--data-urlencode client_secret=$CLIENT_CREDENTIAL \
--data-urlencode scope=email \
--data-urlencode grant_type=client_credentials | jq '.access_token'
```


## Integration with Kong
[Setup Kong](KONG.md)