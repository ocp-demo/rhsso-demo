# Kong API Gateway
<!-- TOC -->

- [Kong API Gateway](#kong-api-gateway)
  - [Build Kong with OpenID plugin](#build-kong-with-openid-plugin)
  - [Setup](#setup)
  - [Configure Kong](#configure-kong)
  - [Configure Kong with RHSSO](#configure-kong-with-rhsso)

<!-- /TOC -->
## Build Kong with OpenID plugin
- Build Kong CentOS as based image and install kong-oidc with [Dockerfile](kong/Dockerfile.kong)
- Kong with oidc alrady built and push to [quay.io/voravitl/kong-oidc:latest](https://quay.io/voravitl/kong-oidc:latest)
## Setup
- Setup PostgreSQL
```bash
NAME=kong-db
KONG_NS=api-gateway
oc new-app \
--name=$NAME \
--template=postgresql-persistent \
-p MEMORY_LIMIT=512Mi \
-p DATABASE_SERVICE_NAME=$NAME \
-p POSTGRESQL_USER=kong \
-p POSTGRESQL_PASSWORD=openshift \
-p POSTGRESQL_DATABASE=kong \
-p VOLUME_CAPACITY=3Gi \
-p POSTGRESQL_VERSION=10 \
-n $KONG_NS
```
- Create Kong Deployment, Service and Route
```bash
oc create -f kong/kong-oidc-deployment.yaml -n $KONG_NS
oc create -f kong/kong-oidc-service.yaml -n $KONG_NS
oc create route edge api --service=kong-oidc --port=proxy -n $KONG_NS
watch oc get pods -n $KONG_NS
POD=$(oc get pods -n $KONG_NS --no-headers | grep kong-oidc | grep Running|head -n 1| awk '{print $1}')
KONG_PROXY_URL=https://$(oc get route/api -n $KONG_NS -o jsonpath='{.spec.host}')
echo "Kong Proxy URL: $KONG_PROXY_URL"
```
- Deploy sample application
```bash
 ./deploy_apps.sh
```
## Configure Kong
- Create Kong's backend's service
```bash   
APP_SERVICE=frontend
APP_NS=user1
oc exec $POD -c proxy -n $KONG_NS -- curl -s -X POST http://localhost:8001/services -d name=$APP_SERVICE -d url=http://$APP_SERVICE.$APP_NS.svc.cluster.local:8080 | jq '.id'
```    

Record id for next step. Following show sample of full JSON Output
```json
    {
        "host":"frontend.user1.svc.cluster.local",
        "created_at":1602148489,
        "connect_timeout":60000,
        "id":"47949afd-6af2-4f07-a4b8-8a837a6aaa03",
        "protocol":"http",
        "name":"frontend",
        "read_timeout":60000,
        "port":8080,
        "path":null,
        "updated_at":1602148489,
        "retries":5,
        "write_timeout":60000,
        "tags":null,
       "client_certificate":null
    }
```
- Create Kong's Route
```bash
    SERVICE_ID=62d2cf5f-7ea2-4e5a-abf1-459491769a8d
    oc exec $POD -c proxy -n $KONG_NS -- curl -s -X POST http://localhost:8001/routes -d service.id=$SERVICE_ID -d "paths[]=/frontend"
    ```
    Sample Output
    ```json
        {
            "id":"ff7c76e1-7112-4590-8613-083b572446a9",
            "path_handling":"v0",
            "paths":[
                "\/frontend"
            ],
            "destinations":null,
            "headers":null,
            "protocols":[
                "http",
                "https"
            ],
            "methods":null,
            "snis":null,
            "service":{
                "id":"47949afd-6af2-4f07-a4b8-8a837a6aaa03"
            },
            "name":null,
            "strip_path":true,
            "preserve_host":false,
            "regex_priority":0,
            "updated_at":1602148567,
            "sources":null,
            "hosts":null,
            "https_redirect_status_code":426,
            "tags":null,
            "created_at":1602148567
        }
```
- Test
  ```bash
  curl -v https://$(oc get route/api -n $KONG_NS -o jsonpath='{.spec.host}')/frontend
  ```
  
## Configure Kong with RHSSO
- Login to RHSSO Web Admin Console and import [keycloak/client-kong.json](keycloak/client-kong.json)
  
    | Parameter        | Value           | 
    | ------------- |:-------------:| 
     | Access Type    | Confidential     |
    |Root URL     | $KONG_PROXY_URL |
    | Valkid Redirect URLs    | /frontend/*      |
    
 - Record secret in Credentials tab
```bash
CLIENT_SECRET=b63a5ed4-6aca-4ed4-a902-b0b8754a1ac4
```
- Check for OpenID configurator
```bash
REALM=demo
curl -s $KEYCLOAK_URL/realms/$REALM/.well-known/openid-configuration
```
- Configure Kong to use oidc plugin
```bash
CLIENT_ID=kong
CLIENT_SECRET=7c2555e3-b59d-4544-9589-283a7d21de2e
oc exec $POD -c proxy -n $KONG_NS -- curl -s -X POST \
http://localhost:8001/plugins \
    -d name=oidc \
    -d config.client_id=$CLIENT_ID \
    -d config.client_secret=$CLIENT_SECRET \
    -d config.discovery=$KEYCLOAK_URL/realms/$REALM/.well-known/openid-configuration
```
Sample output
```json
        {
            "created_at":1602149674,
            "config":{
                "response_type":"code",
                "introspection_endpoint":null,
                "filters":null,
                "bearer_only":"no",
                "ssl_verify":"no",
                "session_secret":null,
                "introspection_endpoint_auth_method":null,
                "realm":"kong",
                "redirect_after_logout_uri":"\/",
                "scope":"openid",
                "token_endpoint_auth_method":"client_secret_post",
                "logout_path":"\/logout",
                "client_id":"kong",
                "client_secret":"d0dfae0c-d9f5-4a07-b4a6-4b9350e7fe97",
                "discovery":"https:\/\/keycloak:8443\/auth\/realms\/demo\/.well-known\/openid-configuration",
                "recovery_page_path":null,
                "redirect_uri_path":null
            },
            "id":"b6b77638-d94d-44e4-a830-ae39075bf7b9",
            "service":null,
            "enabled":true,
            "protocols":[
                "grpc",
                "grpcs",
                "http",
                "https"
            ],
            "name":"oidc",
            "consumer":null,
            "route":null,
            "tags":null
        }
```

- Use browser connect to $KONG_PROXY_URL/frontend and login with user jdoe

