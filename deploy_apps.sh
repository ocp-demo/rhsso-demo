#!/bin/bash
APP_NAMESPACE=user1
oc new-project $APP_NAMESPACE
oc apply -f app/backend-v1-deployment.yaml -n $APP_NAMESPACE
oc apply -f app/backend-v2-deployment.yaml -n $APP_NAMESPACE
oc apply -f app/frontend-v1-deployment.yaml -n $APP_NAMESPACE
oc apply -f app/backend-service.yaml -n $APP_NAMESPACE
oc apply -f app/frontend-service.yaml -n $APP_NAMESPACE
oc apply -f app/frontend-route.yaml -n $APP_NAMESPACE
